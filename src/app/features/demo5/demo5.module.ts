import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Demo5RoutingModule } from './demo5-routing.module';
import { Demo5Component } from './demo5.component';
import { UserPipe } from './pipes/user.pipe';
import { UserComponent } from './components/user.component';


@NgModule({
  declarations: [
    Demo5Component,
    UserPipe,
    UserComponent
  ],
  imports: [
    CommonModule,
    Demo5RoutingModule
  ]
})
export class Demo5Module { }
