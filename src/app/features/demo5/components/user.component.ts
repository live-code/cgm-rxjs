import { Component, Input, OnInit } from '@angular/core';
import { User } from '../../../model/user';
import { Todo } from '../../../model/todo';

@Component({
  selector: 'cgm-user',
  template: `
    <h1>{{data.user.name}}</h1>
    <li *ngFor="let todo of data.todos">{{todo.title}}</li>
    <hr>
  `,
  styles: [
  ]
})
export class UserComponent implements OnInit {
  @Input() data: { user: User, todos: Todo[] };

  constructor() { }

  ngOnInit(): void {
  }

}
