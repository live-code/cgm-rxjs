import { Pipe, PipeTransform } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../../../model/user';
import { map } from 'rxjs/operators';

@Pipe({
  name: 'user'
})
export class UserPipe implements PipeTransform {
  constructor(private http: HttpClient) {

  }

  transform(id: number): Observable<string> {
    return this.http.get<User>('https://jsonplaceholder.typicode.com/users/' + id)
      .pipe(
        map(user => user.name)
      );
  }

}
