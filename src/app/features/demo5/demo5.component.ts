import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { User } from '../../model/user';
import { concatMap, delay, map, mergeMap, switchMap, tap, toArray } from 'rxjs/operators';
import { Todo } from '../../model/todo';

const API = "https://jsonplaceholder.typicode.com";

@Component({
  selector: 'cgm-demo5',
  template: `
    <h1>{{id | user | async}}</h1>
    <button (click)="id = id + 1">inc</button>
    <cgm-user *ngFor="let u of users" [data]="u"></cgm-user>
  `,
})
export class Demo5Component {
  id = 1;
  users: { user: User, todos: Todo[] }[] = [];

  constructor(private http: HttpClient) {

    http.get<User[]>(`${API}/users`)
      .pipe(
        switchMap(users => users),
        concatMap(
          user => this.http.get<Todo[]>(`${API}/todos?userId=${user.id}`)
            .pipe(
              delay(600),
              map(todos => ({ user, todos }))
            )
        ),
        // toArray()
      )
      .subscribe(user => {
        this.users.push(user)
      });

  }
}
