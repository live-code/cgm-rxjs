import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { RandomUser, Result } from '../../model/random-user';
import { combineLatest, iif, interval, Observable, of, timer } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

const API = "https://randomuser.me/api";
const POLLING_TIME = 3000;


@Component({
  selector: 'cgm-demo6',
  template: `
    <select [formControl]="genderInput">
      <option [value]="''">Select a gender to start polling user</option>
      <option value="female">Female</option>
      <option value="male">Male</option>
    </select>

    <hr>
    {{ user?.name.last }}
    <img [src]="user?.picture.thumbnail" />
  `,
  styles: [
  ]
})
export class Demo6Component implements OnInit {
  genderInput: FormControl = new FormControl('');
  user: Result;

  constructor(http: HttpClient) {
    const polling$ = (gender: string): Observable<Result> => timer(0, POLLING_TIME)
      .pipe(
        switchMap(() => http.get<RandomUser>(`${API}?gender=${gender}`)),
        map(user => user.results[0])
      );

    this.genderInput.valueChanges
      .pipe(
        switchMap(gender => iif(() => gender !== '', polling$(gender), of(null)))
      )
      .subscribe(val => this.user = val);
  }

  ngOnInit(): void {
  }

}
