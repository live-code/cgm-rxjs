import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Demo6Component } from './demo6.component';

const routes: Routes = [{ path: '', component: Demo6Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Demo6RoutingModule { }
