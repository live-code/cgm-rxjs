import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { combineLatest, fromEvent, of } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, map, switchMap, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'cgm-demo4forms',
  template: `
    <form [formGroup]="form">
      <input type="text" formControlName="city">
      <input type="checkbox" formControlName="unit"> Is Metric?
    </form>
    
    <div *ngIf="data?.meteo else nodata">
      <pre>{{data | json}}</pre>
    </div>
    
    <ng-template #nodata>
      no results
    </ng-template>
  `,
})
export class Demo4formsComponent {
  data: { city: string, meteo: any }
  form: FormGroup;

  constructor(http: HttpClient, fb: FormBuilder) {

    this.form = fb.group({
      city: '',
      unit: ''
    });

    this.form.valueChanges
      .pipe(
        debounceTime(1000),
        distinctUntilChanged(),
        map(values => ({
          ...values,
          unit: values.unit ? 'metric' : 'imperial'
        })),
        switchMap(
          ({ city, unit  }) => http.get<any>(`http://api.openweathermap.org/data/2.5/weather?q=${city}&units=${unit}&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
            .pipe(
              catchError(err => of(null))
            )
        )
      )
      .subscribe(
        (meteo: any) => this.data = { city: this.form.value.city, meteo },
        err => console.log('errore!', err)
      );

    this.form.setValue({ city: 'Milano', unit: true });

  }
}
