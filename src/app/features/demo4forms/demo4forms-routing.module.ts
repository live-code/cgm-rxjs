import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Demo4formsComponent } from './demo4forms.component';

const routes: Routes = [{ path: '', component: Demo4formsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Demo4formsRoutingModule { }
