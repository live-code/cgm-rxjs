import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Demo4formsRoutingModule } from './demo4forms-routing.module';
import { Demo4formsComponent } from './demo4forms.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    Demo4formsComponent
  ],
  imports: [
    CommonModule,
    Demo4formsRoutingModule,
    ReactiveFormsModule
  ]
})
export class Demo4formsModule { }
