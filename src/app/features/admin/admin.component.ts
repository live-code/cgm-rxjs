import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'cgm-admin',
  template: `
    <p>
      admin works!
    </p>
  `,
  styles: [
  ]
})
export class AdminComponent implements OnInit {

  constructor(private http: HttpClient) {
    http.get('https://jsonplaceholder.typicode.com/xxusers')
      .subscribe(
        res =>  console.log(res),
        err => console.log('errore::::', err)
      );
  }

  ngOnInit(): void {
  }

}
