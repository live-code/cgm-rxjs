import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Post } from '../../model/post';
import { User } from '../../model/user';
import { delay } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'cgm-demo2',
  template: `
    
    <ng-container *ngIf="(user$ | async) as user; else loading">
      <div>
         <pre>{{ user.name}}</pre>
         <pre>{{ user.username}}</pre>
         <pre>{{ user.id}}</pre>
      </div>
      {{user.id}}
    </ng-container>
    
    <ng-template #loading>
      loading....
    </ng-template>
  `,
  styles: [
  ]
})
export class Demo2Component {
  user$: Observable<User> = this.http.get<User>('https://jsonplaceholder.typicode.com/users/1')
    .pipe(
      delay(environment.production ? 0 : 1000)
    );

  constructor(private http: HttpClient) {}

}
