import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { User } from '../../model/user';
import { combineLatest, forkJoin } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { Todo } from '../../model/todo';

@Component({
  selector: 'cgm-demo3',
  template: `
    <pre>{{data?.user | json}}</pre>
    
    <li *ngFor="let todo of data?.todos">
      {{todo.title}}
    </li>
    
    <button (click)="gotoNextUser()">Next</button>
  `,
})
export class Demo3Component {
  data: { user: User, todos: Todo[] };

  constructor(
    private activatedRoute: ActivatedRoute,
    private http: HttpClient,
    private router: Router
  ) {
    /// sequential
    /*
    activatedRoute.params
      .pipe(
        switchMap(({ id }) => http.get<User>('https://jsonplaceholder.typicode.com/users/' + id)),
        switchMap(
          (user) => http.get<Todo[]>('https://jsonplaceholder.typicode.com/todos?userId=' + user.id)
            .pipe(
              map(todos => ({ user, todos}))
            )
        ),
      )
      .subscribe((data) => {
        this.data = data;
      });
      */

    activatedRoute.params
      .pipe(
        map(params => params.id),
        switchMap(id => forkJoin({
          user: http.get<User>(`https://jsonplaceholder.typicode.com/users/${id}`),
          todos: http.get<Todo[]>(`https://jsonplaceholder.typicode.com/todos?userId=${id}`),
        }))
      )
      .subscribe(res => {
        this.data = res;
      })

  }

  gotoNextUser(): void {
    this.router.navigateByUrl('demo3/3')
  }
}
