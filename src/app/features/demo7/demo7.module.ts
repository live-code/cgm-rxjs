import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Demo7RoutingModule } from './demo7-routing.module';
import { Demo7Component } from './demo7.component';
import { ProducerComponent } from './components/producer.component';
import { ReceiverComponent } from './components/receiver.component';


@NgModule({
  declarations: [
    Demo7Component,
    ProducerComponent,
    ReceiverComponent
  ],
  imports: [
    CommonModule,
    Demo7RoutingModule
  ]
})
export class Demo7Module { }
