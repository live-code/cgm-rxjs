import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cgm-demo7',
  template: `
    <cgm-producer></cgm-producer>
    <hr>
    <cgm-receiver></cgm-receiver>
  `,
  styles: [
  ]
})
export class Demo7Component implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
