import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject, Subscription } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { User } from '../../../model/user';
import { map, share, shareReplay, withLatestFrom } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class DataService {
  data$: BehaviorSubject<User[]> = new BehaviorSubject<User[]>([]);

  constructor(private http: HttpClient) {}

  getUsers(): void {
    this.http.get<User[]>('https://jsonplaceholder.typicode.com/users')
      .subscribe(res => this.data$.next(res));
  }

  deleteUser(id: number): void {
    this.http.delete('https://jsonplaceholder.typicode.com/users/' + id)
      .pipe(
        withLatestFrom(this.data$),
        map(values => values[1])
      )
      .subscribe(users => {
        this.data$.next(
          users.filter(u => u.id !== id)
        );
      });
  }
}
