import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';

@Component({
  selector: 'cgm-producer',
  template: `
    
    Users
    
    <li *ngFor="let item of dataService.data$ | async">
      {{item.name}}
      <button (click)="dataService.deleteUser(item.id)">del</button>
    </li>
  `,
  styles: [
  ]
})
export class ProducerComponent implements OnInit {

  constructor(public dataService: DataService) {
    this.dataService.getUsers();
  }

  ngOnInit(): void {
    this.dataService.data$
      .subscribe(val => console.log('success 1', val));

    setTimeout(() => {
      this.dataService.data$
        .subscribe(val => console.log('success 2', val));
    }, 1000);
  }

}
