import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GmapComponent } from './pipes/gmap.component';
import { GmapPipe } from './pipes/gmap.pipe';


@NgModule({
  declarations: [
    LoginComponent,
    GmapComponent,
    GmapPipe
  ],
  imports: [
    CommonModule,
    LoginRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class LoginModule { }
