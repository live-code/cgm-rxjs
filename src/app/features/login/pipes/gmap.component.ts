import { ChangeDetectionStrategy, Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';

@Component({
  selector: 'cgm-gmap',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <img [src]="city | gmap" alt="">
  `,
})
export class GmapComponent implements OnChanges {
  @Input() city: string;
  @Input() zoom: string;
  url: string;

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.city && changes.city.currentValue) {
      this.url = 'https://maps.googleapis.com/maps/api/staticmap?center=' + changes.city.currentValue + '&zoom=5&size=200x100&key=AIzaSyDSBmiSWV4-AfzaTPNSJhu-awtfBNi9o2k';
    }
  }

}
