import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'gmap',
})
export class GmapPipe implements PipeTransform {

  transform(city: string): string {
    return 'https://maps.googleapis.com/maps/api/staticmap?center=' + city + '&zoom=5&size=200x100&key=AIzaSyDSBmiSWV4-AfzaTPNSJhu-awtfBNi9o2k';
  }

}
