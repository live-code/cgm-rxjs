import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { AuthService } from '../../core/auth/auth.service';

@Component({
  selector: 'cgm-login',
  template: `
   
    <input type="text" ngModel>
    <input type="text" ngModel>
    <button (click)="loginHandler()">Login</button>
    
    <h2 *ngIf="authService.getIsLogged$() | async">
      Hello {{authService.displayname$ | async}}
      <button (click)="authService.logout()">quit</button>
    </h2>
    
    <hr>
    <img [src]="city | gmap" alt="">
    <cgm-gmap [city]="city"></cgm-gmap>
    <hr>
    <button (click)="city = 'Trieste'">TS</button>
    <button (click)="city = 'Rome'">RM</button>
  `,
})
export class LoginComponent implements OnInit {
  city = 'Milan';
  constructor(public authService: AuthService) { }

  ngOnInit(): void {
  }

  loginHandler(): void {
    const username = 'pippo';
    const password = '123456';
    this.authService.login(username, password)
  }
}
