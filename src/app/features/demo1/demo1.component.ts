import { Component  } from '@angular/core';
import { UsersService } from './services/users.service';
import { PostsService } from './services/posts.service';
import { TodosService } from './services/todos.service';


@Component({
  selector: 'cgm-demo1',
  template: `
    todos: {{todosService.total$ | async | json}}
    <hr>
    todos completed: {{todosService.completed$ | async | json}}
    <hr>
    users: {{usersService.data$ | async}}
    <hr>
    posts: {{postsService.posts$ | async}}
    posts: {{postsService.getFilteredPost$ | async}}
    <hr>
    total posts: {{postsService.getTotal$ | async}}
    
  `,
})
export class Demo1Component {
  constructor(
    public usersService: UsersService,
    public postsService: PostsService,
    public todosService: TodosService
  ) {
    todosService.init();
  }
}
