import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Post } from '../../../model/post';
import { first, map } from 'rxjs/operators';


@Injectable({ providedIn: 'root' })
export class PostsService {
  posts$: Observable<Post[]> = this.http.get<Post[]>('https://jsonplaceholder.typicode.com/posts/')

  getFilteredPost$: Observable<Post[]> = this.posts$
    .pipe(
      map(posts => posts.filter(p => p.id < 10))
    );

  getTotal$: Observable<number> = this.posts$
    .pipe(
      map(posts => posts.length)
    );

  constructor(private http: HttpClient) { }

}
