import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from '../../../model/user';
import { map, switchMap, toArray } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Todo } from '../../../model/todo';

export type MyUser = {
  displayName: string;
  address: string;
};

@Injectable({ providedIn: 'root' })
export class TodosService {
  todos$: BehaviorSubject<Todo[]> = new BehaviorSubject<Todo[]>([]);

  total$ = this.todos$
    .pipe(
      map(todos => todos.length)
    );

  completed$ = this.todos$
    .pipe(
      map(todos => todos.filter(t => t.completed))
    );

  constructor(private http: HttpClient) {}

  init(): void {
    this.http.get<Todo[]>('https://jsonplaceholder.typicode.com/todos')
      .subscribe(res => {
        this.todos$.next(res);
      });
  }


}
