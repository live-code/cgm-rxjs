import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../../../model/user';
import { map, switchMap, toArray } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

export type MyUser = {
  displayName: string;
  address: string;
};

@Injectable({ providedIn: 'root' })
export class UsersService {
  data$: Observable<MyUser[]> = this.http.get<User[]>('https://jsonplaceholder.typicode.com/users/')
    .pipe(
      /*map(users => users.map(u => ({
        displayName: `${u.name} (${u.username})`,
        address: `${u.address.city} - ${u.address.suite}`
      })))*/

      switchMap(users => users),
      map(user => ({
        displayName: `${user.name} (${user.username})`,
        address: `${user.address.city} - ${user.address.suite}`
      })),
      toArray()

    );

  constructor(private http: HttpClient) {}
}
