import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './core/auth/auth.guard';

const routes: Routes = [
  { path: 'demo1', loadChildren: () => import('./features/demo1/demo1.module').then(m => m.Demo1Module) },
  { path: 'demo2', loadChildren: () => import('./features/demo2/demo2.module').then(m => m.Demo2Module) },
  { path: 'demo3/:id', loadChildren: () => import('./features/demo3/demo3.module').then(m => m.Demo3Module) },
  { path: 'demo4forms/:city', loadChildren: () => import('./features/demo4forms/demo4forms.module').then(m => m.Demo4formsModule) },
  { path: 'demo5', loadChildren: () => import('./features/demo5/demo5.module').then(m => m.Demo5Module) },
  { path: 'demo6', loadChildren: () => import('./features/demo6/demo6.module').then(m => m.Demo6Module) },
  { path: 'demo7', loadChildren: () => import('./features/demo7/demo7.module').then(m => m.Demo7Module) },
  { path: 'login', loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule) },
  { path: 'admin', canActivate: [AuthGuard], loadChildren: () => import('./features/admin/admin.module').then(m => m.AdminModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
