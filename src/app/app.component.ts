import { Component } from '@angular/core';

@Component({
  selector: 'cgm-root',
  template: `
    <cgm-navbar></cgm-navbar>
    <hr>
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class AppComponent {
  title = 'cgm-rjxs';
}
