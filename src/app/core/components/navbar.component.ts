import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cgm-navbar',
  template: `
    <button routerLink="login">login</button>
    <button routerLink="admin" *cgmIflogged>admin</button>
    <button routerLink="demo1">demo1</button>
    
   
  `,
  styles: [
  ]
})
export class NavbarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

