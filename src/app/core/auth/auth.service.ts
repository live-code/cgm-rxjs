import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Auth } from './auth';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class AuthService {
  auth$: BehaviorSubject<Auth> = new BehaviorSubject<Auth>(null);

  constructor(private http: HttpClient) {
    const auth: Auth = JSON.parse(localStorage.getItem('auth'));
    this.auth$.next(auth);
  }

  login(username, password): void {
    const params = new HttpParams()
      .set('user', username)
      .set('pass', password);

    this.http.get<Auth>('http://localhost:3000/login', { params })
      .subscribe(res => {
        this.auth$.next(res);
        localStorage.setItem('auth', JSON.stringify(res))
      });
  }

  logout(): void {
    this.auth$.next(null);
    localStorage.removeItem('auth');
  }

  getIsLogged$(): Observable<boolean> {
    return this.auth$
      .pipe(
        map(auth => !!auth )
      );
  }

  get displayname$(): Observable<string> {
    return this.auth$
      .pipe(
        map(auth => auth?.displayName)
      );
  }

  get token$(): Observable<string> {
    return this.auth$
      .pipe(
        map(auth => auth?.token)
      );
  }

  get role$(): Observable<string> {
    return this.auth$
      .pipe(
        map(auth => auth?.role)
      );
  }
}
