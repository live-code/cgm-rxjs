export interface Auth {
  token: string;
  role: 'admin' | 'moderator';
  displayName: string;
}
