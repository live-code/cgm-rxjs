import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { iif, Observable, of, throwError } from 'rxjs';
import { AuthService } from './auth.service';
import { catchError, first, map, switchMap, withLatestFrom } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService, private router: Router) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.authService.getIsLogged$()
      .pipe(
        first(),
        withLatestFrom(this.authService.token$),
        switchMap(([isLogged, tk]) => iif(
          () => isLogged,
          next.handle(req.clone({ setHeaders: { Authorization: 'Bearer ' + tk } })),
          next.handle(req)
        )),
        catchError((err) => {
          if ( err instanceof HttpErrorResponse ) {
            switch (err.status) {
              case 404:
                console.log('errore 404');
                break;
              case 401:
              default:
                this.router.navigateByUrl('login')
            }
          }
          return throwError(err);
        })
      );
  }

}



