import { Directive, OnDestroy, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthService } from './auth.service';
import { distinctUntilChanged, filter, takeUntil, tap } from 'rxjs/operators';
import { Subject, Subscription } from 'rxjs';

@Directive({
  selector: '[cgmIflogged]'
})
export class IfloggedDirective implements OnDestroy {
  destroy$ = new Subject();

  constructor(
    private viewContainerRef: ViewContainerRef,
    private template: TemplateRef<any>,
    private authService: AuthService
  ) {
    authService.getIsLogged$()
      .pipe(
        tap(val => console.log(val)),
        distinctUntilChanged(),
        takeUntil(this.destroy$),
        tap(() => viewContainerRef.clear()),
        filter(isLogged => isLogged)
      )
      .subscribe(() => {
        viewContainerRef.createEmbeddedView(template)
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
